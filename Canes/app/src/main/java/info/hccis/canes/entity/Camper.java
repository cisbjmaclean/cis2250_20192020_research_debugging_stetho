package info.hccis.canes.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import info.hccis.canes.util.Util;

/**
 * This class will represent a camper.  It will be used for loading the data from the json string.
 * As well, it has the annotations needed to allow it to be used for the Room database.
 * @since 20200202
 * @author BJM
 */

@Entity(tableName = "camper")
public class Camper {

//    public static final String CAMPER_BASE_API = Util.BASE_SERVER+"canes/rest/CamperService/";
//    public static final String CAMPER_BASE_API = Util.BASE_SERVER+"canes/rest/CamperService/";
    public static final String CAMPER_BASE_API = Util.BASE_SERVER+"canes/rest/CamperService/";

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "firstName")
    private String firstName;
    @ColumnInfo(name = "lastName")
    private String lastName;
    @ColumnInfo(name = "dob")
    private String dob;
    @ColumnInfo(name = "campType")
    private Integer campType;
    @ColumnInfo(name = "campTypeDescription")
    private String campTypeDescription;



    public String getCampTypeDescription() {
        return campTypeDescription;
    }

    public void setCampTypeDescription(String campTypeDescription) {
        this.campTypeDescription = campTypeDescription;
    }



    public Camper() {
    }

    public Camper(Integer id) {
        this.id = id;
    }

    public Camper(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getCampType() {
        return campType;
    }

    public void setCampType(Integer campType) {
        this.campType = campType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Camper)) {
            return false;
        }
        Camper other = (Camper) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "#"+id+" Name: "+this.firstName+" "+this.lastName;
    }

}
