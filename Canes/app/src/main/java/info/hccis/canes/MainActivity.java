package info.hccis.canes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.view.Menu;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import info.hccis.canes.bo.CamperContent;
import info.hccis.canes.bo.UserContent;
import info.hccis.canes.broadcast.receivers.MyBroadcastReceiver;
import info.hccis.canes.dao.MyAppDatabase;
import info.hccis.canes.entity.Camper;
import info.hccis.canes.ui.camperdetail.CamperDetailsFragment;
import info.hccis.canes.ui.camperlist.CamperFragment;
import info.hccis.canes.ui.camperlist.CamperListFragment;
import info.hccis.canes.ui.tools.ToolsFragment;
import info.hccis.canes.util.NotificationApplication;
import info.hccis.canes.util.NotificationUtil;

public class MainActivity extends AppCompatActivity implements
        ToolsFragment.OnFragmentInteractionListener,
        CamperListFragment.OnListFragmentInteractionListener,
        CamperFragment.OnFragmentInteractionListener,
        CamperDetailsFragment.OnFragmentInteractionListener {

    private AppBarConfiguration mAppBarConfiguration;
    public static MyAppDatabase myAppDatabase;
    private static NavController navController = null;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NotificationApplication.setContext(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //bjm 20200208 removing fab
//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send, R.id.nav_campers_list, R.id.nav_camper)
                .setDrawerLayout(drawer)
                .build();

        //BJM 20200131
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        //BJM 20200131 Build the app database object
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "canesdb").allowMainThreadQueries().build();

        //Test the Room functionality (this is now done in CamperContent when loading campers
        //CamperContent.getCampersFromRoom();

        IntentFilter filters = new IntentFilter();
        filters.addAction(Intent.ACTION_SCREEN_ON);
        broadcastReceiver = new MyBroadcastReceiver();
        registerReceiver(broadcastReceiver, filters);

        /*
          BJM 20200207 This is done when the campers are actually loaded.
          NotificationUtil.sendNotification();
        */


        /*
           BJM 20200204 Shared Preferences
           Can use shared preferences to store key/value pairs which can be useful in your apps.
         */

        //Shared Preferences
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        boolean preferToLoad = sharedPref.getBoolean(getString(R.string.preference_load_from_room), false);
        Log.d("bjm", "read from shared pref:" + preferToLoad);

        // BJM 20200207 This is implemented in the Tools fragment toggle button now. Left here for sample coding.
        //        SharedPreferences.Editor editor = sharedPref.edit();
        //        editor.putBoolean((getString(R.string.preference_load_from_room)), true);
        //        editor.commit();
        //        Log.d("bjm", "saved preference");

        preferToLoad = sharedPref.getBoolean(getString(R.string.preference_load_from_room), true);
        Log.d("bjm", "read from shared pref:" + preferToLoad);


        //Test the user access rest access
        //test load the users
//        UserContent.loadUsers();
//        UserContent.validateUser();

    }

    public static NavController getNavController() {
        return navController;
    }

    /**
     * Adding onDestroy to allow receiver to be unregistered.  This was giving an exception when
     * using the back button.
     *
     * @author bjm
     * @since 20200207
     */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Are you missing a call to unregisterReceiver()?
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This method will allow the menu (top right) to function to switch between fragments
     * similar to the navigation drawer.
     *
     * @param item
     * @return
     * @author BJM
     * @since 20200202
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.onNavDestinationSelected(item, navController)
                || super.onOptionsItemSelected(item);
    }

    /**
     * This method will allow the navigation drawer to function to switch between fragments based
     * on which item is chosen in the nav drawer.
     *
     * @return
     * @author BJM
     * @since 20200202
     */

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * This method will be used in the camper fragment when the user clicks on a row of the
     * camper recyclerview.  This method will transfer the user to a details fragment.  The
     * id of the camper will be passed to the fragment and used to load the correct camper details.
     * Will use the arraylist associated with the recyclerview.
     *
     * @param item the camper
     * @author BJM
     * @since 20200124
     */
    @Override
    public void onListFragmentInteraction(Camper item) {

        /*
            BJM 20200131
            Send the user to a details fragment.
        */

        Log.d("bjm", "item communicated from fragment: " + item.toString());


        /* BJM 20200202
           Put the json representation of the camper into the bundle to be passed to the fragment.
           This will be used in the details fragment.
        */

        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        bundle.putString("camper", gson.toJson(item));

        /*
          BJM 20200202
          Use the navigation controller object stored as an attribute of the main
          activity to nagivate the ui to the camper detail fragment.
        */

        getNavController().navigate(R.id.nav_camper_detail, bundle);

    }


    /**
     * Base onFragmentInteraction method (Not used)
     *
     * @author BJM
     * @since 20200131
     */

    @Override
    public void onFragmentInteraction() {

    }

    /**
     * This fragment interaction will occur when the user wants to send details to a camper.
     *
     * @param textToSend
     * @param emailAddress
     * @author BJM
     * @since 20200131
     */
    @Override
    public void onFragmentInteractionSendEmail(String textToSend, String emailAddress) {
        Log.d("bjm", "in main activity successfully-" + textToSend + "-" + emailAddress);

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
        email.putExtra(Intent.EXTRA_SUBJECT, "Canes notification");
        email.putExtra(Intent.EXTRA_TEXT, textToSend);

        //need this to prompts email client only
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));

    }

    /**
     * This is the interaction from the details fragment.  I will want to add a new contact when
     * this button is pressed.
     *
     * @param item
     * @author BJM
     * @since 20200203
     */

    @Override
    public void onFragmentInteractionAddContact(Camper item) {

        /* BJM 20200131 Code to add a contact */
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, item.getFirstName() + " " + item.getLastName());
        startActivity(intent);

//        navController.navigate(R.id.nav_home);
    }

    /**
     * This is the interaction from the details fragment.  I will want to add a new contact when
     * this button is pressed.
     *
     * @param item
     * @author BJM
     * @since 20200203
     */

    @Override
    public void onFragmentInteractionSendEmail(Camper item) {

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_SUBJECT, "Canes notification");
        email.putExtra(Intent.EXTRA_TEXT, item.toString());

        //need this to prompts email client only
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));


    }


}
