package info.hccis.canes.bo;

import android.util.Log;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.ArrayList;
import java.util.List;

import info.hccis.canes.MainActivity;
import info.hccis.canes.entity.Camper;
import info.hccis.canes.entity.UserAccess;
import info.hccis.canes.ui.camperlist.CamperListFragment;
import info.hccis.canes.util.JsonCamperApi;
import info.hccis.canes.util.JsonUserAccessApi;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class UserContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<UserAccess> USERS = new ArrayList<UserAccess>();




    /**
     * Load the campers.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the CAMPERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadUsers() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UserAccess.BASE_API)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonUserAccessApi jsonUserAccessApi = retrofit.create(JsonUserAccessApi.class);

        //Create a list of campers.
        Call<List<UserAccess>> call = jsonUserAccessApi.getUsers();

        call.enqueue(new Callback<List<UserAccess>>() {

            @Override
            public void onResponse(Call<List<UserAccess>> call, Response<List<UserAccess>> response) {

                if (!response.isSuccessful()) {
                    Log.d("bjm", "Code" + response.code());
                    return;
                }

                List<UserAccess> users = response.body();
                Log.d("bjm", "data back from service call # users returned=" + users.size());
                for(UserAccess current: users){
                    Log.d("bjm users back:", current.toString());
                }



                //**********************************************************************************
                // Now that we have the campers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                UserContent.USERS.clear();
                UserContent.USERS.addAll(users);

            }

            @Override
            public void onFailure(Call<List<UserAccess>> call, Throwable t) {

                //**********************************************************************************
                // If the api call failed, give a notification to the user.
                //**********************************************************************************

                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());
            }
        });
    }



//    public static void validateUser() {
//
//        //Use Retrofit to connect to the service
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(UserAccess.BASE_API)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        JsonUserAccessApi jsonUserAccessApi = retrofit.create(JsonUserAccessApi.class);
//
//        //Create a list of campers.
//        String username = "";
//        String password = "";
//        String credentials = "cis2232_fitness,bj.maclean@gmail.com,123";
////        Call<Integer> call = jsonUserAccessApi.loginUser(credentials);
//        Call<Integer> call = jsonUserAccessApi.loginUser(credentials);
//
//        call.enqueue(new Callback<Integer>() {
//
//            @Override
//            public void onResponse(Call<Integer> call, Response<Integer> response) {
//
//                if (!response.isSuccessful()) {
//                    Log.d("bjm", "Code from login service" + response.code());
//                    return;
//                }
//
//                Log.d("bjm response body", response.body().toString());
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Integer> call, Throwable t) {
//
//                //**********************************************************************************
//                // If the api call failed, give a notification to the user.
//                //**********************************************************************************
//
//                Log.d("bjm", "login service api call failed");
//                Log.d("bjm", t.getMessage());
//            }
//        });
//    }




}