package info.hccis.canes.ui.camperdetail;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import info.hccis.canes.R;
import info.hccis.canes.entity.Camper;
import info.hccis.canes.ui.camperlist.CamperListFragment;

public class CamperDetailsFragment extends Fragment {

    private Camper camper;
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvDOB;
    private ImageButton buttonAddContact;
    private ImageButton buttonSendEmail;
    private OnFragmentInteractionListener mListener;


    public static CamperDetailsFragment newInstance() {
        return new CamperDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          Note how the arguments are passed from the activity to this fragment.  The
          camper object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String camperJson = getArguments().getString("camper");
            Gson gson = new Gson();
            camper = gson.fromJson(camperJson, Camper.class);
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.camper_details_fragment, container, false);

        tvFirstName = view.findViewById(R.id.textViewDetailFirstName);
        tvLastName = view.findViewById(R.id.textViewDetailLastName);
        tvDOB = view.findViewById(R.id.textViewDetailDOB);

        tvFirstName.setText(camper.getFirstName());
        tvLastName.setText(camper.getLastName());
        tvDOB.setText(camper.getDob());

        buttonAddContact = view.findViewById(R.id.imageButtonAdd);

        /*
          BJM 20200203
          When the add button is clicked, will pass the camper object to the activity implementation
          method.  The MainActivity will then take care of adding the contact.
         */

        buttonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionAddContact(camper);
            }
        });

        /*
          BJM 20200203
          Similar to the add contact button, to send an email will notify the MainActivity and
          let it take care of sending the email with the information passed in the camper.
         */

        buttonSendEmail = view.findViewById(R.id.imageButtonSend);
        buttonSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionSendEmail(camper);
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CamperListFragment.OnListFragmentInteractionListener) {
            mListener = (CamperDetailsFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionAddContact(Camper item);
        void onFragmentInteractionSendEmail(Camper camper);
    }
}
