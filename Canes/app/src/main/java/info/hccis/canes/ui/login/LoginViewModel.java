package info.hccis.canes.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Log;
import android.util.Patterns;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import info.hccis.canes.bo.UserContent;
import info.hccis.canes.entity.UserAccess;
import info.hccis.canes.login.LoginRepository;
import info.hccis.canes.login.Result;
import info.hccis.canes.login.model.LoggedInUser;
import info.hccis.canes.R;
import info.hccis.canes.util.JsonUserAccessApi;
import info.hccis.canes.util.Util;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(final String username, String password) {
        // can be launched in a separate asynchronous job
        //Result<LoggedInUser> result = loginRepository.login(username, password);

        //******************************************************************************************
        //BJM 20200207
        // Using Retrofit to connect to login.  Note that the loginResult is updated
        // in the success.  The observer will notice that this is updated and react accordingly.
        //******************************************************************************************

        Log.d("bjm login","logging in username="+username+" pw="+password);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UserAccess.BASE_API)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonUserAccessApi jsonUserAccessApi = retrofit.create(JsonUserAccessApi.class);

        /*
         BJM 20200207
         Note that I am connecting to the fitness database here for authentication testing.  The
         Canes service is down so connecting to fitness (service works the same for authentication.
         */

        String credentials = Util.DATABASE_NAME+","+username+","+password;
        Log.d("bjm login", "logging in with credentials="+credentials);
        Call<Integer> call = jsonUserAccessApi.loginUser(credentials);

        call.enqueue(new Callback<Integer>() {

            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                //If not successful connection fail with message
                if (!response.isSuccessful()) {
                    Log.d("bjm", "Code from login service" + response.code());
                    loginResult.setValue(new LoginResult(R.string.login_failed_connect));
                    return;
                }

                //Was able to connect but username/pw was not correct.
                if(response.body().toString().equals("0")){
                    loginResult.setValue(new LoginResult(R.string.login_failed));
                    Log.d("bjm login", "login result was 0");
                }else {
                    //Successful login.
                    Log.d("bjm response body", response.body().toString());
                    loginResult.setValue(new LoginResult(new LoggedInUserView(username)));
                }

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

                //**********************************************************************************
                // If the api call failed, give a notification to the user.
                //**********************************************************************************

                Log.d("bjm", "login service api call failed");
                Log.d("bjm", t.getMessage());
                loginResult.setValue(new LoginResult(R.string.login_failed_connect));
            }
        });












//        if (result instanceof Result.Success) {
//            LoggedInUser data = ((Result.Success<LoggedInUser>) result).getData();
//            loginResult.setValue(new LoginResult(new LoggedInUserView(data.getDisplayName())));
//        } else {
//            loginResult.setValue(new LoginResult(R.string.login_failed));
//        }
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 2;
    }
}
